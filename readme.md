# Documentation de l'installation Chelsea

## Préparation

Il faut 4 raspberry Pi clients et 1 pour le serveur.
On flash les cartes SD de chaques cartes avec une image contenant l'OS (ex raspbian);
Le plus simple est de préparer une première cartes SD, de la paramétrer et ensuite de la cloner.

Il faut installer sur le système :

1. Git
2. Nodejs, une version récente
3. Npm
4. PM2 via npm
5. Chromium

ensuite il faut :

1. Cloner le dépôt :
$ git clone git@framagit.org:jeremien/B563-Zebra-bordeaux.git

2. aller dans le depôt et installer les paquets npm :
$ cd /nom_du_depot && npm install

3. lancer l'application :
$ node app.js

4. pour que l'application rédémarre avec les cartes, il faut utiliser PM2 (voir
la doc de PM2 http://pm2.keymetrics.io/)

5. il faut paramétrer le fichier de configuration de chromium en mode kiosk
(http://blog.philippegarry.com/2016/03/29/faire-de-son-pi-une-borne-raspberry-pi-kiosk-mode-jessie-version/)
et renseigner les adresses de chaques pages pour chaques cartes (ex:
http://192.168.3.5:3000/clientA).

Pour trouver l'adresse ip du serveur, il faut soit taper la commande $ ifconfig, soit en mode graphique, accéder aux informations de la carte réseau.

## Dispositif

Toutes les cartes sont connectés en RJ45 sur un routeur.
Le serveur sert les pages aux autres cartes.
Les cartes clientes se connectent sur le serveur avec Chromium et l'adresse ip
du serveur et le port déclaré dans le code.
La page d'accueil correspond à : http://192.168.43.179:3000/
Le client A (conversation Chelsea - Lamo) : http://192.168.43.179:3000/clientA
Le client B(conversation Chelsea - Lamo) : http://192.168.43.179:3000/clientB
Le client C (banque d'image) : http://192.168.43.179:3000/clientC
Le client C (banque d'image) : http://192.168.43.179:3000/clientD

Lorsque qu'une carte cliente démarre, elle lance l'OS (raspbian), chromium
démarre en mode kiosk (plein écran) et se connecte avec l'url renseignée
dans le fichier de configuration de l'autostart
(~/.config/autostart/autoChromium.desktop). Elle lance donc une requête au
serveur pour qu'il lui serve la page avec les données correspondantes. Le
serveur doit donc être déjà prêt.

Il n'y a que la carte serveur qui fait tourner l'application avec node et
éventuellement un logiciel de monitoring comme pm2 s'il faut automatiser le
prcocessus.

Pour que le serveur démarre en premier, on peut utiliser des minuteurs et
décaler de 10 minutes le démarrage de la carte qui sert de serveur.

Chaque carte a la même adresse ip (celle du serveur) mais une url différente
(l'ip avec le port + le chemin).
