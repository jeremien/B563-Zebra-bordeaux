// declaration des modules utilisés
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const LineByLine = require('line-by-line');
//const expressLess = require('express-less');
//const less = require("less");
//var _ = require('underscore');

// declaration des variables
// lignes par lignes
//

/* ------------ declaration home ---------------------------------- */
var txtHome = new LineByLine('./public/txt/home.txt');
// stockage des données
var lineHome = "";
let tableauHome =[];

/* ------------ declaration client A 192.168.1.3/clientA ---------- */
var txtClientA = new LineByLine('./public/txt/textA.txt');
// stockage des données
var lineA = "";
let tableauA =[];

/* ------------ declaration client B 192.168.1.2/clientB ---------- */

var txtClientB = new LineByLine('./public/txt/textB.txt');
// stockage des données
var lineB = "";
let tableauB =[];

/* ------------ declaration client C ---------- */

var txtClientC = new LineByLine('./public/txt/textC.txt');
// stockage des données
var lineC = "";
let tableauC =[];

/* ------------ declaration client C ---------- */

var txtclientD = new LineByLine('./public/txt/textD.txt');
// stockage des données
var lineD = "";
let tableauD =[];

/* ------------------ global --------------------*/

// gestion du temps
var time = 0;
var time2 = 5000;

/* ----- connection clients -------- */

// gestion users
let user = [];

//stockage des clients pour vérification
/*
io.on('connection', function (socket) {
        socket.on('message', function(client){
          client = client.data;
          console.log(client + ' est connecté');
          user.push(client);
        });
        var boolean = user.includes('clientA' && 'clientB');
        if(boolean === true){
          console.log(user + ' sont dans le tableau ' + boolean + ' est donnée');
          socket.emit('start', {
            data : boolean
          });
        }
});
*/


// déclaration des routes express + dossier statique
app.use(express.static(__dirname + '/public'));
//app.use('/css', expressLess(__dirname + '/less'));


/* ------------------- routes -------------------- */

// home
app.get('/', function(req, res, next) {
    res.render('home.jade', {
      'title': 'home'
    });
});
// client A
app.get('/clientA', function(req, res, next) {
    res.render('clientA.jade', {
      'title': 'clientA'
    });
});
// client B
app.get('/clientB', function(req, res, next) {
    res.render('clientB.jade', {
      'title': 'clientB'
    });
});

// client C
app.get('/clientC', function(req, res, next) {
    res.render('clientC.jade', {
      'title': 'clientC'
    });
});

// client D
app.get('/clientD', function(req, res, next) {
    res.render('clientD.jade', {
      'title': 'clientD'
    });
});


/* ------------------ home ------------------ */

// paramétrage de la lecture ligne par ligne pour le client B
txtHome.on('line', function(lineHome){
  // on active la lecture que si les utilisateurs A et B sont connectés
  txtHome.resume();

  // on envoie les données dans le tableauB
  tableauHome.push(lineHome);
  //console.log('home -->' + lineHome);
});
// on detecte la fin
txtHome.on('end', function(){
  // on lance l'envoie au client
  io.on('connection', function (socket) {

            socket.emit('message_home',{
              data : tableauHome
            });

            // determine le temps commun
            setInterval(function(){
              time = Math.floor((Math.random() * 10) + 20);
              time2 = Math.floor((Math.random() * 10000) + 6000);
              //console.log('vitesse  --> ' + time);
              //console.log('vitesse2  --> ' + time2);
              socket.emit('temps',{
                data : time,
                data2 : time2
              });
              socket.broadcast.emit('temps',{
                data : time,
                data2 : time2
              });
          }, 2000);

  });
});



/* ------------------ client A ------------------ */

// paramétrage de la lecture ligne par ligne pour le client A
txtClientA.on('line', function(lineA){
  // on active la lecture que si les utilisateurs A et B sont connectés
  txtClientA.resume();
  // on envoie les données dans le tableauA
  tableauA.push(lineA);
  //console.log('data --> ' + lineA);
});
// on detecte la fin
txtClientA.on('end', function(){
  // on lance l'envoie au client
  io.on('connection', function (socket) {

          socket.emit('message_clientA', {
              data : tableauA
            });

  });

});

/* ------------------ client B ------------------ */


// paramétrage de la lecture ligne par ligne pour le client B
txtClientB.on('line', function(lineB){
  // on active la lecture que si les utilisateurs A et B sont connectés
  txtClientB.resume();

  // on envoie les données dans le tableauB
  tableauB.push(lineB);
});
// on detecte la fin
txtClientB.on('end', function(){
  // on lance l'envoie au client
  io.on('connection', function (socket) {

            socket.emit('message_clientB',{
              data : tableauB
            });

  });
});

/* ------------------ client C ------------------ */


// paramétrage de la lecture ligne par ligne pour le client C
txtClientC.on('line', function(lineC){
  // on active la lecture que si les utilisateurs A et B sont connectés
  txtClientC.resume();

  // on envoie les données dans le tableauB
  tableauC.push(lineC);
});
// on detecte la fin
txtClientC.on('end', function(){
  // on lance l'envoie au client
  io.on('connection', function (socket) {

            socket.emit('message_clientC',{
              data : tableauC
            });

  });
});

/* ------------------ client D ------------------ */


// paramétrage de la lecture ligne par ligne pour le client C
txtclientD.on('line', function(lineD){
  // on active la lecture que si les utilisateurs A et B sont connectés
  txtclientD.resume();

  // on envoie les données dans le tableauB
  tableauD.push(lineD);
});
// on detecte la fin
txtclientD.on('end', function(){
  // on lance l'envoie au client
  io.on('connection', function (socket) {

            socket.emit('message_clientD',{
              data : tableauD
            });

  });
});

// declaration du serveur sur le port 3000
server.listen(3000);
console.log('serveur en marche sur 3000');
