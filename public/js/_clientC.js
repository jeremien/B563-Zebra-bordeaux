/* ------- client C --------- */

var timePulse = 0;
var tabPulse = [100, 200, 300, 400, 500];

function pulse(delai){
 $('#message_clientC')
 .animate({
     backgroundColor: "blue"
   }, delai ).animate({
       backgroundColor: "white"
     }, delai);
}

function vitesse(){
  timePulse = Math.floor((Math.random() * 100) + 3000);
}

$(document).ready(function() {


  setInterval(function(){
          vitesse();
          pulse(timePulse);
          console.log(timePulse);
      }, timePulse);
});
