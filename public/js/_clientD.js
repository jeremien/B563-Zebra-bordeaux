/* ------- client D --------- */

var timePulse = 0;

function pulse(delai){
 $('#message_clientD')
 .animate({
     backgroundColor: "blue"
   }, delai ).animate({
       backgroundColor: "white"
     }, delai);
}

function vitesse(){
  timePulse = Math.floor((Math.random() * 100) + 3000);
}

$(document).ready(function() {

  setInterval(function(){
          vitesse();
          pulse(timePulse);
          console.log(timePulse);
      }, timePulse);
});
