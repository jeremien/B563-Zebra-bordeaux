-------------- svg ---------


------------- typo  -------
<div class="titre">hello world</div>
<div class="unicode">&#x2602 &#x2600 &#x26A5</div>


--------------------- couleur --------------------------------
<div class="pulse">hello world</div>
<div class="blink">hello world</div>

--------------------- texte --------------------------------
<div class="chat">hello world</div>
<div class="mot">hello</div>
<div class="phrase">Devant la pression médiatique et politique, votre entreprise </div>

--------------------- texte animé --------------------------
<marquee> hello </marquee>
<marquee direction="up"> hello </marquee>
<div class="text-bounce"><marquee width="250" height="200" direction="down" behavior="alternate"> hello </marquee></div>

--------------------- images -------------------------
<div class="full"><img src="./img/img2.jpg"></div>
<div class="fourchan"><img src="./img/img1.png"></div>
<div class="gif"><img src="./img/img3.gif"></div>
